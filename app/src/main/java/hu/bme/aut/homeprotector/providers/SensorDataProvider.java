package hu.bme.aut.homeprotector.providers;


import java.util.ArrayList;
import java.util.List;

import hu.bme.aut.homeprotector.sensors.MotionDetector;
import hu.bme.aut.homeprotector.sensors.Sensor;

public class SensorDataProvider {

    private static List<Sensor> listOfSensors = new ArrayList<>();
    private static String[] locations = {"Kitchen", "Living room", "Ante-room", "Bedroom", "Basement"};

    static {
        for (int i = 0; i < 5; i++) {
            listOfSensors.add(new MotionDetector(locations[i]));
        }
        for (Sensor sensor : listOfSensors) {
            sensor.turnOn();
        }
    }


    public List<Sensor> getListOfSensors() {
        return listOfSensors;
    }
}

