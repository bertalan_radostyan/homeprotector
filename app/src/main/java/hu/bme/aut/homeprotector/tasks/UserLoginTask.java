package hu.bme.aut.homeprotector.tasks;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents an asynchronous login/registration task used to authenticate
 * the user.
 */
public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
    private HttpURLConnection connection = null;

    private String email;
    private String password;

    private UserLoginTaskListener listener;

    public UserLoginTask(String email, String password,
                         HttpURLConnection connection, UserLoginTaskListener listener) {
        this.email = email;
        this.password = password;

        this.listener = listener;

        this.connection = connection;
    }

    @Override
    public Boolean doInBackground(Void... voids) {
        String message = "{ email: \"" +  email + "\", password: \"" + password + "\" }";

        try {
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(0);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            OutputStream out = new BufferedOutputStream(connection.getOutputStream());
            out.write(message.getBytes());

            StringBuilder builder = new StringBuilder();
            InputStream in = new BufferedInputStream(connection.getInputStream());

            int c = in.read();
            while (c != -1) {
                builder.append((char)c);
                c = in.read();
            }

            Pattern pattern = Pattern.compile("[ ]*[{][ ]*success[ ]*:[ ]*true[ ]*[}][ ]*");
            Matcher matcher = pattern.matcher(builder.toString());
            return matcher.matches();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (listener != null)
            listener.done(success);
    }

    public interface UserLoginTaskListener {
        void done(boolean success);
    }
}
