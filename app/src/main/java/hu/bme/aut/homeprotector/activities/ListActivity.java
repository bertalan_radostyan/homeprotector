package hu.bme.aut.homeprotector.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import hu.bme.aut.homeprotector.R;
import hu.bme.aut.homeprotector.providers.SensorDataProvider;
import hu.bme.aut.homeprotector.sensors.Sensor;

public class ListActivity extends AppCompatActivity {

    private SensorDataProvider sensorDataProvider = new SensorDataProvider();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ArrayAdapter adapter = new ArrayAdapter<Sensor>(this,R.layout.activity_list,sensorDataProvider.getListOfSensors());

        ListView listView = (ListView) findViewById(R.id.list_of_sensors);
        listView.setAdapter(adapter);
    }
}

