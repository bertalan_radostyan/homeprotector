package hu.bme.aut.homeprotector.sensors;

import hu.bme.aut.homeprotector.sensors.SensorState;

public abstract class Sensor {

    SensorState sensorState;
    String location;


    public SensorState getSensorState() {
        return sensorState;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public abstract void turnOn();

    public abstract void turnOff();

    public abstract void arm();

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MotionDetector{");
        sb.append("Location: " + location + "State: " + sensorState);
        return sb.toString();
    }

}
