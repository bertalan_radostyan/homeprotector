package hu.bme.aut.homeprotector.sensors;


public class MotionDetector extends Sensor {

    public MotionDetector(String location) {
        this.location = location;
        sensorState = SensorState.IDLE;
    }

    @Override
    public void turnOn() {
        this.sensorState = SensorState.ONLINE;
    }

    @Override
    public void turnOff() {
        this.sensorState = SensorState.OFFLINE;
    }

    @Override
    public void arm() {

    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MotionDetector)){
            return false;
        }

        MotionDetector anotherMotionDetector = (MotionDetector) obj;

        return (this.sensorState == anotherMotionDetector.sensorState && this.location.equals(anotherMotionDetector.location));

    }
}
