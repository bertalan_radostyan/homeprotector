package hu.bme.aut.homeprotector;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hu.bme.aut.homeprotector.providers.SensorDataProvider;
import hu.bme.aut.homeprotector.sensors.MotionDetector;
import hu.bme.aut.homeprotector.sensors.Sensor;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ListUnitTest {

    private static String[] locations = {"Kitchen", "Living room", "Ante-room", "Bedroom", "Basement"};

    @Test
    public void testGetListOfSensorsIsNotNull(){
        SensorDataProvider sensorDataProvider = new SensorDataProvider();
        assertNotNull(sensorDataProvider.getListOfSensors());
    }

    @Test
    public void testGetListOfSensorsEqualsToExpectedList(){
        List<Sensor> expectedListOfSensors = new ArrayList<>();
        for (String location : locations) {
            expectedListOfSensors.add(new MotionDetector(location));
        }
        for (Sensor sensor : expectedListOfSensors) {
            sensor.turnOn();
        }
        assertArrayEquals(expectedListOfSensors.toArray(),(new SensorDataProvider().getListOfSensors().toArray()));
    }

    @Test
    public void testGetListOfSensorsIsNotEmpty(){
        assertNotEquals(0,new SensorDataProvider().getListOfSensors().size());
    }
}
