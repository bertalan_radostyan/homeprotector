package hu.bme.aut.homeprotector;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Mock implementation of HttpURLConnection class required for testing.
 */
public class TestHttpURLConnection extends HttpURLConnection {
    private String response;

    protected TestHttpURLConnection(URL u) {
        super(u);
    }

    public TestHttpURLConnection(String response) {
        super(null);

        this.response = response;
    }

    @Override
    public InputStream getInputStream() {
        return new InputStream() {
            private int i = 0;

            @Override
            public int read() throws IOException {
                if (i == response.length())
                    return -1;

                return response.charAt(i++);
            }
        };
    }

    @Override
    public OutputStream getOutputStream() {
        return new OutputStream() {
            @Override
            public void write(int i) throws IOException {

            }
        };
    }

    @Override
    public void setDoOutput(boolean b) {

    }

    @Override
    public void disconnect() {

    }

    @Override
    public boolean usingProxy() {
        return false;
    }

    @Override
    public void connect() throws IOException {

    }
}