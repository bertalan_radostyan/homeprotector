package hu.bme.aut.homeprotector;

import org.junit.Test;

import hu.bme.aut.homeprotector.activities.LoginActivity;
import hu.bme.aut.homeprotector.tasks.UserLoginTask;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for methods and classes related to user login.
 */
public class LoginUnitTest {
    //Class: LoginActivity

    //Method: isUsernameValid(String)

    //Tests the method with an empty string.
    @Test
    public void testIsUsernameValidWithEmpty() {
        String email = "";

        assertFalse(LoginActivity.isUsernameValid(email));
    }

    //Tests the method with a string containing a capital letter and a number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingNumberEnding() {
        String username = "A1";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Tests the method with a string containing a capital letter, a lowercase letter and a number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingLetterFollowingNumberEnding() {
        String username = "Aa1";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Tests the method with a string containing a capital letter, a lowercase and a letters
    // and a number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingLettersFollowingNumberEnding() {
        String username = "AaA1";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Tests the method with a string containing a capital, a capital and a lowercase letters
    // and a number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingLettersFollowingNumberEnding2() {
        String username = "AAa1";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Tests the method with a string containing a capital, a lowercase, a capital and a
    // lowercase letter and a number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingLettersFollowingNumberEnding3() {
        String username = "AaAa1";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Tests the method with a string containing a capital, a number and another number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingNumberFollowingNumberEnding() {
        String username = "A21";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Tests the method with a string containing a capital, a number, a letter and another number.
    @Test
    public void TestIsUserNameValidWithCapitalStartingNumberAndLetterFollowingNumberEnding() {
        String username = "Aa21";

        assertTrue(LoginActivity.isUsernameValid(username));
    }

    //Method isPasswordValid(String)

    //Tests the method with a valid password: 123Asd
    @Test
    public void testIsPasswordValidWithValid() {
        String password = "123Asd";

        assertTrue(LoginActivity.isPasswordValid(password));
    }

    //Tests the method with an empty string.
    @Test
    public void testIsPasswordValidWithEmpty() {
        String password = "";

        assertFalse(LoginActivity.isPasswordValid(password));
    }

    //Tests the method with an invalid password: asdAsd
    @Test
    public void testIsPasswordValidWithNoNumber() {
        String password = "asdAsd";

        assertFalse(LoginActivity.isPasswordValid(password));
    }

    //Tests the method with an invalid password: 123123
    @Test
    public void testIsPasswordValidWithNoLetter() {
        String password = "123123";

        assertFalse(LoginActivity.isPasswordValid(password));
    }

    //Tests the method with an invalid password: 123asd
    @Test
    public void testIsPasswordValidWithNoCapital() {
        String password = "123asd";

        assertFalse(LoginActivity.isPasswordValid(password));
    }

    //Class: UserLoginTask

    //Tests the class with the connection returning success.
    @Test
    public void testUserLoginTaskWithSuccess() {
        TestHttpURLConnection connection = new TestHttpURLConnection("{ success: true }");
        UserLoginTask task = new UserLoginTask("", "", connection, null);
        assertTrue(task.doInBackground());
    }

    //Tests the class with the connection returning not success.
    @Test
    public void testUserLoginTaskWithNotSuccess() {
        TestHttpURLConnection connection = new TestHttpURLConnection("{ success: false }");
        UserLoginTask task = new UserLoginTask("", "", connection, null);
        assertFalse(task.doInBackground());
    }

    //Tests the class with the connection returning invalid response.
    @Test
    public void testUserLoginTaskWithInvalidResponse() {
        TestHttpURLConnection connection = new TestHttpURLConnection("gibberish");
        UserLoginTask task = new UserLoginTask("", "", connection, null);
        assertFalse(task.doInBackground());
    }
}
